#!/bin/bash
set -o nounset  # Treat unset variables as an error

pwd

source ./main.conf



# 1. Disabling Firewalld / NetworkManager & Enabling sshd / Network Service
# -> ./net_if_setup.sh  : eth0 configuration / ifup eth0
# -> ./wifi_if_setup.sh : Wifi Adapter configuration / ifup wlp0s6u1
# -> ./quagga_setup.sh  : zebrad / ospfd Setup
#   -> ./zebra_template.conf
#   -> ./ospfd_template.conf

if [[ $var_1 == "on" ]]; then
  sh ./network_setup.sh
else
  continue
fi




# 2. Setup hostname / domainanme / FQDN with Current IP Address

if [[ $var_2 == "on" ]]; then
  sh ./hostname_setup.sh
else
  continue
fi




# 3. Base packages installation / tools / VM guest Additions

if [[ $var_3 == "on" ]]; then
  sh ./base_configuration.sh
else
  continue
fi



# 4. Disabling SElinux

if [[ $var_4 == "on" ]]; then
  sh ./selinux_setup.sh
else
  continue
fi




# 5. nftables setup

if [[ $var_5 == "on" ]]; then
  sh ./iptables_setup.sh
else
  continue
fi



# 6. nsd Setup
# -> ./nsd_template.conf  : configuration template file

if [[ $var_6 == "on" ]]; then
  sh ./nsd_setup.sh
else
  continue
fi





# 7. unbound Setup
# -> ./unbound_template.conf  : configuration template file

if [[ $var_7 == "on" ]]; then
  sh ./unbound_setup.sh
else
  continue
fi





# 8. dhcpd Setup
# -> ./dhcpd_template.conf  : configuration template file

if [[ $var_8 == "on" ]]; then
  sh ./dhcpd_setup.sh
else
  continue
fi

# hostapd Setup
# -> ./hostapd_template_wpa2.conf  : configuration template file

if [[ $var_9 == "on" ]]; then
  sh ./hostapd_setup.sh
else
  continue
fi





#---------------------------------- Wait for Mail_VM up ----------------------------------#

printf "\n\nPlease Boot up Mail_VM  or  ifup __NIC__ on Mail_VM   at this point...\n"

# Testing variable
declare p_test=1
while [[ $p_test -eq 1 ]]; do

  if [ $(nc -z 10.16.8.1 22; echo $?) -eq 0 ]; then
    # Run Scripts
    echo "SYSTEM: Starting [Remote setup for Mail_VM]..."

    # Remote setup for Mail_VM
    if [[ $var_10 == "on" ]]; then
      sh ./mail_vm_remote_setup.sh
    else
      continue
    fi

    # Set testing variable to 0, so it will break the loop
    p_test=0

  else
    echo “Waiting for [Mail_VM]...”


  fi



done



#---------------------------------- Wait for Mail_VM up ----------------------------------#



# Reboot system
# systemctl reboot --force
