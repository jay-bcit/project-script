#!/bin/bash
set -o nounset  # Treat unset variables as an error

##### selinux_setup.sh #####
# Disable SELINUX

echo "Disabling selinux"
setenforce 0
sed -r -i 's/SELINUX=(enforcing|disabled)/SELINUX=permissive/' /etc/selinux/config
echo "Disabling selinux: Completed"
