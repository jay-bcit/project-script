#!/bin/bash
set -o nounset


declare user_name=$1
declare pass_word=$2
declare router_vm_ip=$3
declare mail_macaddr=$4
declare new_mailuser_name=$5
declare new_mailuser_passwd=$6



ssh $user_name@$router_vm_ip "yum -y install git ; \
                              mkdir Scripts ; \
                              git clone https://jay-bcit@bitbucket.org/jay-bcit/project-script.git Scripts ; \
                              cd Scripts ; \
                              sed -r -i -e \"s/declare mail_macaddr=.*$/declare mail_macaddr=\"$mail_macaddr\"/\" \
                                        -e \"s/declare new_mailuser_name=.*$/declare new_mailuser_name=\"$new_mailuser_name\"/\" \
                                        -e \"s/declare new_mailuser_passwd=.*$/declare new_mailuser_passwd=\"$new_mailuser_passwd\"/\" \
                                        -e \"s/declare ssh_passwd=.*$/declare ssh_passwd=\"$pass_word\"/\" ./main.conf ;
                              sh -x ./main.sh"
