#!/bin/bash
set -o nounset  # Treat unset variables as an error

source ./main.conf
export SSHPASS=$ssh_passwd



# Installing sshpass
yum -y install sshpass



# Upload mail_vm directory + main.conf into MailVM (from Project VM)
#sshpass -e scp -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" ./main.conf $LOGNAME@$(printf $mail_hostip):$HOME/
#sshpass -e scp -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" ./base_configuration.sh $LOGNAME@$(printf $mail_hostip):$HOME/
#sshpass -e scp -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" ./selinux_setup.sh $LOGNAME@$(printf $mail_hostip):$HOME/
cp ./main.conf ./mail_vm/
cp ./base_configuration.sh ./mail_vm/
cp ./selinux_setup.sh ./mail_vm/
sshpass -e scp -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" ./mail_vm/* $LOGNAME@$(printf $mail_hostip):./



# Run main_mail_vm.sh remotely (from Project VM)
sshpass -e ssh -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" $LOGNAME@$(printf $mail_hostip) 'sh ./main_mail_vm.sh'



#---------------------------------- Deprecated Lines -----------------------------------#

# Generating ssh keys
#ssh-keygen -N $new_ssh_pass -f $HOME/.ssh/id_rsa

# Adding .pub into MailVM (from Project VM)
# sshpass -p "$ssh_passwd" ssh -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no"  $LOGNAME@$(printf $mail_hostip) mkdir $HOME/.ssh/authorized_keys
# sshpass -p "$ssh_passwd" scp -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" $HOME/.ssh/authorized_keys/id_rsa.pub $LOGNAME@$(printf $mail_hostip):$HOME/.ssh/id_rsa.pub


# Create mail_user remotly (from Project VM)  => Moved to  postfix_setup.sh
# sshpass -e ssh -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" $LOGNAME@$(printf $mail_hostip) 'useradd -p $(openssl passwd -1 $new_mailuser_passwd) $new_mailuser_name -g mail'

#---------------------------------- Deprecated Lines -----------------------------------#
