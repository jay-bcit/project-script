#!/bin/bash
set -o nounset  # Treat unset variables as an error

# Disabling Firewalld / NetworkManager & Enabling sshd / Network Service
# Disabling Firwalld
echo "Disabling firewalld"
systemctl stop firewalld.service
systemctl disable firewalld.service
echo "Disabling firewalld Completed"

# Disabling NetworkManager
echo "Disabling Network Manager"
systemctl stop NetworkManager.service
systemctl disable NetworkManager.service
echo "Disabling Network Manager Completed"

# Enabling Network Service
echo "Enabling / Starting Network Service"
systemctl enable network.service
systemctl start network.service
echo "Enabling / Starting Network Service Completed"

# Enabling sshd Service
echo "Enabling sshd"
systemctl enable sshd.service
systemctl start sshd.service
echo "Enabling sshd Completed"
