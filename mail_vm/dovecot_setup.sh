#!/bin/bash
set -o nounset  # Treat unset variables as an error

source ./main.conf


##### dovecot_setup.sh #####
# dovecot setup


# Install dovecot
yum -y install epel-release dovecot


# Starting / Enabling dovecot
systemctl start dovecot
systemctl enable dovecot




## Modifying & Copying template file
sed -r -i "s/login_trusted_networks = .*$/login_trusted_networks = $networkid_enp\/$currentprefix/" ./dovecot_template.conf
cp ./dovecot_template.conf /etc/dovecot/dovecot.cf


## Copying 10-mail-template.conf file
cp ./10-mail-template.conf /etc/dovecot/conf.d/10-mail.conf

## Copying 10-auth-template.conf file
cp ./10-auth-template.conf /etc/dovecot/conf.d/10-auth.conf



# Reload dovecot settings
dovecot reload




# Restart ​ dovecot.service
systemctl restart dovecot
