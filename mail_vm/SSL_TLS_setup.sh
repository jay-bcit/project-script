#!/bin/bash
set -o nounset  # Treat unset variables as an error

source ./main.conf




# SSL/TLS Authentication setup
## Installing crypto-utils
yum -y install crypto-utils mod_ssl

## Generating .crt / .key
sh ./SASL_genkey_setup.sh

## grep .crt / .key file location
declare new_certificate=$(ls /etc/pki/tls/certs | grep $(hostname -f).crt)
declare new_private_key=$(ls /etc/pki/tls/private | grep $(hostname -f).key)


### dostfix: Modifying /etc/postfix/main.cf
sed -r -i "s|smtpd_tls_cert_file = .*$|smtpd_tls_cert_file = /etc/pki/tls/certs/$new_certificate|" /etc/postfix/main.cf
sed -r -i "s|smtpd_tls_key_file = .*$|smtpd_tls_key_file = /etc/pki/tls/private/$new_private_key|" /etc/postfix/main.cf

## reload postfix
postfix reload







#### dovecot: Modifying /etc/dovecot/conf.d/10-ssl.conf
sed -r -i "s|ssl_cert = .*$|ssl_cert = </etc/pki/tls/certs/$new_certificate|" ./10-ssl-template.conf
sed -r -i "s|ssl_key = .*$|ssl_key = </etc/pki/tls/private/$new_private_key|" ./10-ssl-template.conf
cp ./10-ssl-template.conf /etc/dovecot/conf.d/10-ssl.conf

# disabling plaintext auth for SSL: /etc/dovecot/conf.d/10-auth.conf
sed -r -i "s/disable_plaintext_auth = .*$/disable_plaintext_auth = yes/" /etc/dovecot/conf.d/10-auth.conf

## reload dovecot
dovecot reload
