#!/bin/bash
set -o nounset  # Treat unset variables as an error

source ./main.conf

## Gennerating Certificate / Private key
# Using openssl
openssl req -subj "/CN=$(hostname -f)/OU=$OU/O=$O/L=$L/ST=$ST/C=$CNTRY" -new -newkey rsa:2048 -days 365 -nodes -x509 -keyout /etc/pki/tls/private/$(hostname -f).key -out /etc/pki/tls/certs/$(hostname -f).crt 


# Using genkey
#/usr/bin/keyutil -c makecert -g 2048 -s "CN=mail.s08.as.learn, OU=NASP Intake 2018, O=NASP2018, L=Vancouver, ST=British Columbia, C=CA" -v 12 -a -z /etc/pki/tkls.rand.8802 -o /etc/pki/tls/certs/mail.s08.as.learn.crt -k /etc/pki/tls/private/mail.s08.as.learn.key
