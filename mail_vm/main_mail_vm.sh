#!/bin/bash
set -o nounset  # Treat unset variables as an error



# Base packages installation / tools / VM guest Additions
sh ./base_configuration.sh


# Disabling SElinux
sh ./selinux_setup.sh


# Setup hostname / domainanme / FQDN with Current IP Address
sh ./hostname_mail_vm.sh


# Disabling Firewalld / NetworkManager & Enabling sshd / Network Service
sh ./network_mail_vm.sh


# postfix Setup
sh ./postfix_setup.sh

# dovecot Setup
sh ./dovecot_setup.sh

# SASL Setup
sh ./SASL_setup.sh

# SSL/TLS Authentication setup
sh ./SSL_TLS_setup.sh


# Reboot machine
# systemctl reboot --force
