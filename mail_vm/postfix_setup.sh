#!/bin/bash
set -o nounset  # Treat unset variables as an error

source ./main.conf


##### postfix_setup.sh #####
# postfix setup


# To Sync time in mail server
## NTP check
if [[ $(rpm -qa | grep ntp) = "" ]]; then
  yum -y install ntp ntpdate
else
  continue
fi

## Set time zone
timedatectl set-timezone America/Vancouver

## Synchronizing the System Clock with a Remote Server
timedatectl set-ntp yes



# Create user / passwd
# useradd -p $(openssl passwd -1 $new_mailuser_passwd) $new_mailuser_name -g mail
useradd $new_mailuser_name -g mail; echo -e "$new_mailuser_passwd\n$new_mailuser_passwd" | passwd $new_mailuser_name


# Install postfix
yum -y install epel-release postfix

# Starting / Enabling postfix
systemctl start postfix
systemctl enable postfix

## Modifying & Copy template file
sed -r -i "s/myhostname = .*$/myhostname = $mail_hostname.$newdomainname/" ./postfix_template.conf
cp ./postfix_template.conf /etc/postfix/main.cf

## Modifyin /etc/hosts
sed -i '/::1/d' /etc/hosts


# Reload postfix settings
postfix reload



# Restart ​ postfix.service
systemctl restart postfix
