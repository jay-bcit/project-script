#!/bin/bash
set -o nounset  # Treat unset variables as an error

source ./main.conf


# Check if Postfix was compiled with support for Dovecot SASL

if [[ $(postconf -a | grep dovecot) == "dovecot" ]]; then

  # grep postix username and put into 10-master.conf file
  sed -r -i "s/\suser = .*$/    user = $(cat /etc/passwd | grep postfix | cut -d":" -f1)/" ./10-master-template.conf
  sed -r -i "s/\sgroup = .*$/    group = $(cat /etc/passwd | grep postfix | cut -d":" -f1)/" ./10-master-template.conf
  cp ./10-master-template.conf /etc/dovecot/conf.d/10-master.conf


  # Adding SASL configuration block in /etc/postfix/main.cf
  if [[ $(cat /etc/postfix/main.cf | grep "SMTP Settings") == "" ]]; then
    cat ./SASL_template_main.conf >> /etc/postfix/main.cf
  else
    echo "SASL configuration found in /etc/postfix/main.cf... Quiting SASL configuration..."
  fi

  # Using SASL with Postfix submission port
  sed -r -i "s/\s-o smtpd_sasl_local_domain=.*$/ -o smtpd_sasl_local_domain=$mail_hostname.$newdomainname/" ./SASL_template_master.cf
  cp ./SASL_template_master.cf /etc/postfix/master.cf

  # Reload changed configurations for postfix and dovecot
  postfix reload
  dovecot reload

else
  echo "SYSTEM: [WARNING] postfix is not compiled for dovecotSASL"
fi
