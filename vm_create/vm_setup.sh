#!/bin/bash
set -o nounset  # Treat unset variables as an error


# Creating Router_VM
sh vm_create_router.sh

# Creating Mail_VM
sh vm_create_mail.sh
