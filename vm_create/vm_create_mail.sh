#!/bin/bash
set -o nounset  # Treat unset variables as an error

source ./vm_create.conf


# 1. Create VM
vboxmanage createvm --name "$vm_name_mail" --groups "$group_name" -ostype Redhat_64 --register

  # Setup Memory
vboxmanage modifyvm $vm_name_mail --memory 2048
  # Create vhdd
vboxmanage createhd --filename "$group_path/$vm_name_mail/$vm_name_mail.vdi" --size 100000 --format VDI --variant Standard
  # Create SATA
vboxmanage storagectl $vm_name_mail --name "SATA Controller" --add sata --controller IntelAhci
  # Attach vhdd to SATA
vboxmanage storageattach $vm_name_mail --storagectl "SATA Controller" --port 0 --device 0 --type hdd --medium "$group_path/$vm_name_mail/$vm_name_mail.vdi"
  # Create IDE
vboxmanage storagectl $vm_name_mail --name "IDE Controller" --add ide --controller PIIX4
  # Load iso on IDE
vboxmanage storageattach $vm_name_mail --storagectl "IDE Controller" --port 1 --device 0 --type dvddrive --medium "$iso_path"



# 2. NIC setting
vboxmanage modifyvm $vm_name_mail --nic1 intnet --nictype1 virtio --intnet1 $intnet_name --cableconnected2 on

# 4. Start VM
vboxmanage startvm $vm_name_mail
