#!/bin/bash
set -o nounset  # Treat unset variables as an error

source ./vm_create.conf

# 1. Create VM
vboxmanage createvm --name "$vm_name" --groups "$group_name" -ostype Redhat_64 --register

  # Setup Memory
vboxmanage modifyvm $vm_name --memory 2048
  # Create vhdd
vboxmanage createhd --filename "$group_path/$vm_name/$vm_name.vdi" --size 100000 --format VDI --variant Standard
  # Create SATA
vboxmanage storagectl $vm_name --name "SATA Controller" --add sata --controller IntelAhci
  # Attach vhdd to SATA
vboxmanage storageattach $vm_name --storagectl "SATA Controller" --port 0 --device 0 --type hdd --medium "$group_path/$vm_name/$vm_name.vdi"
  # Create IDE
vboxmanage storagectl $vm_name --name "IDE Controller" --add ide --controller PIIX4
  # Load iso on IDE
vboxmanage storageattach $vm_name --storagectl "IDE Controller" --port 1 --device 0 --type dvddrive --medium "$iso_path"



# 2. NIC setting
vboxmanage modifyvm $vm_name --nic1 bridged --bridgeadapter1 $vlan_name --cableconnected1 on
vboxmanage modifyvm $vm_name --nic2 intnet --nictype2 virtio --intnet2 $intnet_name --cableconnected2 on


# 3. USB setting
vboxmanage modifyvm $vm_name --usb on
vboxmanage usbfilter add 1 --target $vm_name --name Wifi_Adapter --manufacturer Ralink

# 4. Start VM
vboxmanage startvm $vm_name
