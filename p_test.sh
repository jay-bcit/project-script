#!/bin/bash
set -o unset

source $HOME/Scripts/main.conf

# Testing variable
declare p_test=1


while (( $p_test == 1 ));
do
  if [[ $(ping -c 1 $mail_hostip | grep icmp* | wc -l) == 0  ]]; then
    echo “Waiting for [Mail_VM]...”



  else

    # Run Scripts
    echo "SYSTEM: Starting [Remote setup for Mail_VM]..."

    # Remote setup for Mail_VM
    if [[ $var_10 == "on" ]]; then
      sh mail_vm_remote_setup.sh
    else
      continue
    fi

    # Set testing variable to 0, so it will break the loop
    p_test=0


  fi

done
