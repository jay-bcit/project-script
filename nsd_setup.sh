#!/bin/bash
set -o nounset  # Treat unset variables as an error

source ./main.conf


##### nsd_setup.sh #####
# nsd setup



# Install nsd
## epel-release existing test -> if null, install epel-release before install nsd
if [[ $(rpm -qa | grep epel-release) = "" ]]; then
  yum -y install epel-release
  yum -y install nsd
else
  yum -y install nsd
fi

# Enabling / Starting nsd
systemctl enable nsd.service
systemctl start nsd.service

# 1.Modifying nsd_template.conf
sed -r -i "s/\sip-address:\s\b[0-9]{1,3}.\b[0-9]{1,3}.\b[0-9]{1,3}.\b[0-9]{1,3}/ ip-address: $currentip/" ./nsd_template.conf


## Add Forward zone / Reverse zone

sed -r -i "s/newdomainname/$newdomainname/" ./nsd_template.conf
sed -r -i "s/reverse_lookupzone_eth/$reverse_lookupzone_eth/" ./nsd_template.conf
## Overide nsd.conf
cp ./nsd_template.conf /etc/nsd/nsd.conf




# 2.Creating Forward zonefile
sed -r -i "s/newdomainname/$newdomainname/" ./forward_zone_template.zone
sed -r -i "s/newhostname/$newhostname/" ./forward_zone_template.zone
sed -r -i "s/mail_hostname/$mail_hostname/" ./forward_zone_template.zone
sed -r -i "s/admin_email/$admin_email/" ./forward_zone_template.zone
sed -r -i "s/currentip/$currentip/" ./forward_zone_template.zone
sed -r -i "s/mail_hostip/$mail_hostip/" ./forward_zone_template.zone
## Copy template to /etc/nsd
cp ./forward_zone_template.zone /etc/nsd/$newdomainname.zone




# 3.Creating Reverse zonefile
sed -r -i "s/newdomainname/$newdomainname/" ./reverse_zone_template.zone
sed -r -i "s/newhostname/$newhostname/" ./reverse_zone_template.zone
sed -r -i "s/mail_hostname/$mail_hostname/" ./reverse_zone_template.zone
sed -r -i "s/admin_email/$admin_email/" ./reverse_zone_template.zone
sed -r -i "s/reverse_lookupzone_eth/$reverse_lookupzone_eth/" ./reverse_zone_template.zone
sed -r -i "s/reverse_lookupzone_mail_hostip/$reverse_lookupzone_mail_hostip/" ./reverse_zone_template.zone
sed -r -i "s/networkid_eth/$networkid_eth/" ./reverse_zone_template.zone

## Copy template to /etc/nsd
cp ./reverse_zone_template.zone /etc/nsd/$reverse_lookupzone_eth.zone


## Restart nsd.service
systemctl restart nsd.service
