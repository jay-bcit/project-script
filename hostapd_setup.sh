#!/bin/bash
set -o nounset  # Treat unset variables as an error

source ./main.conf


##### hostapd_setup.sh #####
# hostapd setup



# Install hostapd
if [[ $(rpm -qa | grep hostapd) = "" ]]; then
  yum -y install hostapd
else
  continue
fi



# 1.Modifying hostapd_template_wpa2.conf
sed -r -i "s/wpa_passphrase=.*$/wpa_passphrase=$wpa2_passwd/" ./hostapd_template_wpa2.conf
sed -r -i "s/interface=.*$/interface=$interface_name_wlp/" ./hostapd_template_wpa2.conf
sed -r -i "s/ssid=.*$/ssid=$new_essid/" ./hostapd_template_wpa2.conf
sed -r -i "s/channel=.*$/channel=$new_channel/" ./hostapd_template_wpa2.conf
sed -r -i "s/ignore_broadcast_ssid=.*$/ignore_broadcast_ssid=$essid_broadcast_opt/" ./hostapd_template_wpa2.conf





## Overide hostapd.conf
cp ./hostapd_template_wpa2.conf /etc/hostapd/hostapd.conf


# Enabling / Starting hostapd
systemctl enable hostapd.service
systemctl start hostapd.service


## Restart dependencies
systemctl restart unbound.service
systemctl restart ospfd.service

## Restart daemon
systemctl restart hostapd.service
