#!/bin/bash
set -o nounset  # Treat unset variables as an error

source ./main.conf


##### net_if_setup.sh #####
# $interface_ifcfg_eth file configurations

## We assuming that enp0s3 is already exist
echo "IP Addressing Starting - 2nd Ethernet Adapter"


## Setup for eth0
# Create new ifcfg file for eth0
touch $interface_ifcfg_eth

# Change Device name
sed -r "s/$interfacename/$interface_name_eth/" $interfaceifcfg > $interface_ifcfg_eth


# ONBOOT = "yes"
sed -r -i 's/ONBOOT="(yes|no|)"/ONBOOT="yes"/' $interface_ifcfg_eth
# Set DEFROUTE = no
sed -r -i 's/DEFROUTE="(yes|no|)"/DEFROUTE="no"/' $interface_ifcfg_eth

# Delete UUID line
sed -i '/^UUID/d' $interface_ifcfg_eth

# Delete DNS Server lines
sed -i '/^DNS1/d' $interface_ifcfg_eth
sed -i '/^DNS2/d' $interface_ifcfg_eth

# IP Addressing
sed -r -i "s/IPADDR=\"\b[0-9]{1,3}.\b[0-9]{1,3}.\b[0-9]{1,3}.\b[0-9]{1,3}\"/IPADDR="$newip_eth"/" $interface_ifcfg_eth
sed -r -i "s/GATEWAY=\"\b[0-9]{1,3}.\b[0-9]{1,3}.\b[0-9]{1,3}.\b[0-9]{1,3}\"/GATEWAY=$newgateway_eth/" $interface_ifcfg_eth
sed -r -i "s/PREFIX=\"\b[0-9]{1,2}\"/PREFIX=$newsubnet_eth/" $interface_ifcfg_eth


echo "IP Addressing Result"
echo "-New IP Address: $newip_eth"
echo "-New IP Subnet: $newsubnet_eth"
echo "-New IP DG: $newgateway_eth"
echo "IP Addressing Completed"


# Turn on / Restart interface
if [[ $(cat /sys/class/net/$interface_name_eth/operstate) == "up" ]]; then
  ifdown $interface_name_eth
  ifup $interface_name_eth
  echo "$interface_name_eth has been UP"

else
  ifup $interface_name_eth
  echo "$interface_name_eth has been UP"
fi
