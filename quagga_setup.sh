#!/bin/bash
set -o nounset  # Treat unset variables as an error

source ./main.conf


##### quagga_setup.sh #####
# quagga setup



# Install quagga
if [[ $(rpm -qa | grep quagga) = "" ]]; then
  yum -y install quagga
else
  continue
fi



# 1.Modifying zebra_template.conf
sed -r -i "s/\scurrent_date.*$/$(date --rfc-3339=seconds)/" ./zebra_template.conf

sed -r -i "s/interface\senp_INTFACE/interface $interfacename/" ./zebra_template.conf
sed -r -i "s/\sdescription enp_DESCRPT/\tdescription $description_net_enp/" ./zebra_template.conf
sed -r -i "s/\sip address enp_IP_PRFX/\tip address $currentip\/$currentprefix/" ./zebra_template.conf

sed -r -i "s/interface\seth_INTFACE/interface $interface_name_eth/" ./zebra_template.conf
sed -r -i "s/\sdescription eth_DESCRPT/\tdescription $description_net_eth/" ./zebra_template.conf
sed -r -i "s/\sip address eth_IP_PRFX/\tip address $currentip_eth\/$currentprefix_eth/" ./zebra_template.conf

sed -r -i "s/interface\swlp_INTFACE/interface $interface_name_wlp/" ./zebra_template.conf
sed -r -i "s/\sdescription wlp_DESCRPT/\tdescription $description_net_wlp/" ./zebra_template.conf
sed -r -i "s/\sip address wlp_IP_PRFX/\tip address $currentip_wlp\/$currentprefix_wlp/" ./zebra_template.conf


## Overide zebra.conf
cp ./zebra_template.conf /etc/quagga/zebra.conf


# Enabling / Starting zebra
systemctl enable zebra.service
systemctl start zebra.service

# Restarting zebra
systemctl restart zebra.service











# 2.Modifying ospfd_template.conf
sed -r -i "s/\scurrent_date.*$/$(date --rfc-3339=seconds)/" ./ospfd_template.conf


sed -r -i "s/interface\senp_INTFACE/interface $interfacename/" ./ospfd_template.conf
sed -r -i "s/interface\seth_INTFACE/interface $interface_name_eth/" ./ospfd_template.conf
sed -r -i "s/interface\swlp_INTFACE/interface $interface_name_wlp/" ./ospfd_template.conf


sed -r -i "s/\srouter-id.*$/ router-id $currentip/" ./ospfd_template.conf
sed -r -i "s/\snetwork\senp_NETID/\tnetwork $networkid_enp\/$currentprefix area $area_num/" ./ospfd_template.conf
sed -r -i "s/\snetwork\seth_NETID/\tnetwork $networkid_eth\/$currentprefix_eth area $area_num/" ./ospfd_template.conf
sed -r -i "s/\snetwork\swlp_NETID/\tnetwork $networkid_wlp\/$currentprefix_wlp area $area_num/" ./ospfd_template.conf


## Overide ospfd.conf
cp ./ospfd_template.conf /etc/quagga/ospfd.conf

# Permission setting for /etc/quagga/ospfd.conf
chown quagga:quagga /etc/quagga/ospfd.conf

# Enabling / Starting ospfd
systemctl enable ospfd.service
systemctl start ospfd.service

# Restarting ospfd
systemctl restart ospfd.service
