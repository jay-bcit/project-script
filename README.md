# Application Services Content - [Project]

*****


[`Jay Jang`](https://jay-bcit@bitbucket.org/jay-bcit/Project-Script) - NASP 2018 / BCIT



*****

## Agenda

**This repository contains scripts that will create two VMs and setup all services by configuring itself remotely from your HostMachine or manually from VMs**

+ Pre-requisities
> Initial VM setup - [ VM Creation scripts hierarchy ]

+ How to use?
> Running instructions or notes need to execute any of your scripts + example

+ Understanding `main.sh`
> Useful information - [ `main.sh` hierarchy ] [ `main_mail_vm.sh` hierarchy ]

*****

## Pre-requisities

#### 1. Run `vm_setup.sh` to create two new Virtual Machines

 + Router_Vm
 + Mail_VM

#

    !. USAGE - `vm_setup.sh`

        (1) Provide information to  `vm_create.conf`

          - Image file (.iso) PATH                            # Mounting image file on CD-ROM while VM created
                Line 14: iso_path="__ISO_FILE_PATH__"


-----------------------

#### [VM Creation scripts hierarchy ]

###### /vm_create

    vm_setup.sh                      # Main script for VM creation on Host_Machine
      ./vm_create.conf               # Variables
      ./vm_create_router.sh          # [Router VM] Creation script
      ./vm_create_mail.sh            # [Mail VM] Creation script



-----------------------


#### 2. Configure 1st NIC on [Router_VM] while installing OS

- In order to have internet access so you can download scripts by `git clone <repository>`
#
      [Router_VM]

      Example)
      - OS              : CentOS_7 Installed
      - NIC             : Bridged to VLAN2016
      - USB             : USB filter added on VM (Wifi Adapter)
      - IP		        : 10.16.255.8 / 24
      - DG		        : 10.16.255.254
      - DNS1	    	: 142.232.221.253
      - DNS2	    	: 8.8.8.8
      - Hostname		: localhost
      - User		   	: root / toor

#



#### 3. [Mail_VM] does not need any pre-configuration



#
-----------------------



## How to use?

#

#### 1. Run script `router_vm_remote_setup.sh` on Host_Machine



     !. USAGE - `router_vm_remote_setup.sh`



        sh router_vm_remote_setup.sh <router_vm_user_name> <router_vm_pass_word> <router_vm_ip_address> <mail_vm_mac_address> <new_mailuser_name> <new_mailuser_passwd>




        * Example)

            sh router_vm_remote_setup.sh root toor 10.16.255.188 "08:00:27:12:FA:AA" nasp nasp





        * Explanation)

            This scripts is remote setup script for both [Router_VM] & [Mail_VM].
              - In order to provide correct information to `main.conf`,
                Please pass all arguments in order.


                <router_vm_user_name>     : User name that will run script on [Router_VM]   => `root`
                <router_vm_pass_word>     : Password of <router_vm_user_name>               => `toor`
                <router_vm_ip_address>    : Main NIC IP Address on [Router_VM]              => `10.16.255.188`
                <mail_vm_mac_address>     : Main NIC MAC Address on [Mail_VM]               => `To obtain ip address from dhcpd`
                <new_mailuser_name>       : Create new user account on [Mail_VM]            => `Thunderbird Mail User Account`
                <new_mailuser_passwd>     : Password of <new_mailuser_name>                 => `Thunderbird Mail User Password`






        * How does Script works?)



          (1) When scripts runs by user on Host_machine, it will execute commands on [Router_VM] remotely.

            ssh $user_name@$router_vm_ip "yum -y install git ; \
                                          mkdir ~/Scripts ; \
                                          git clone git clone https://jay-bcit@bitbucket.org/jay-bcit/project-script.git ~/Scripts ; \
                                          sed -r -i -e \"s/declare mail_macaddr=.*$/declare mail_macaddr=\"$mail_macaddr\"/\" \
                                                    -e \"s/declare new_mailuser_name=.*$/declare new_mailuser_name=\"$new_mailuser_name\"/\" \
                                                    -e \"s/declare new_mailuser_passwd=.*$/declare new_mailuser_passwd=\"$new_mailuser_passwd\"/\" \
                                                    -e \"s/declare ssh_passwd=.*$/declare ssh_passwd=\"$pass_word\"/\" ~/main.conf ;
                                          sh -x ~/main.sh"




          (2) Notice that last line of this script will execute `main.sh` on [Router_VM].


              - A. In order to turn on / off to install (Run each `_setup` script) each service, modify `main.conf` on [Router_VM]
                   By default, all services & ip routing is enabled.


              [ Toggle switch in `main.conf`]
              # ----------------------------------------------------------------------- #
              #
              #     Toggle switch to run scripts
              #                 .---------------------------------- 01. network_setup.sh
              #                 |   .------------------------------ 02. hostname_setup.sh
              #                 |  |  .---------------------------- 03. base_configuration.sh
              #                 |  |  |   .------------------------ 04. selinux_setup.sh
              #                 |  |  |  |  .---------------------- 05. iptables_setup.sh
              #                 |  |  |  |  |  .------------------- 06. nsd_setup.sh
              #                 |  |  |  |  |  |  .---------------- 07. unbound_setup.sh
              #                 |  |  |  |  |  |  |  .------------- 08. dhcpd_setup.sh
              #                 |  |  |  |  |  |  |  |  .---------- 09. hostapd_setup.sh
              #                 |  |  |  |  |  |  |  |  |  .------- 10. mail_vm_remote_setup.sh
              #                 |  |  |  |  |  |  |  |  |  |  .---- 11. net.ipv4.ip_forward on / off
              #                 |  |  |  |  |  |  |  |  |  |  |
              #                 *  *  *  *  *  *  *  *  *  *  *
              declare tgswitch="on on on on on on on on on on on"
              #
              # ----------------------------------------------------------------------- #








              - B. This script will also trigger to run `mail_vm_remote_setup.sh` that reside in same folder with `main.sh`


                                          [ Scripts Flow ]

              `router_vm_remote_setup.sh`                                       # Runs on [Host_Machine]

                              ->       `main.sh`                                # Runs on [Router_VM]

                                        ->    `mail_vm_remote_setup.sh`         # Runs on [Router_VM]

                                                  ->    `main_mail_vm.sh`       # Runs on [Mail_VM]






          (3) At the last line on `main.sh` script,

              - In order to accomplish this step, [Mail_VM] will need to obtain IP address from `dhcpd` on [Router_VM].

              - Script will ping to [Mail_VM] to check if `mail_hostip` is alive and wait for [Mail_VM] until it's up.




                * To obtain IP address automatically on [Mail_VM], NIC must be up or run a command on [Mail_VM]:

                                               `ifup <NIC>`



                  - If you want to skip this step, configure NIC on[Mail_VM] to have internet access
                    with the IP address that you provided in `main.conf`

                    -> 1) [Router_VM] will runs script `mail_vm_remote_setup.sh`
                    -> 2) This script will uploads `/mail_vm` directory with `main.conf` file to [Mail_VM]
                    -> 3) Then, it'll trigger to run `/mail_vm/ main_mail_vm.sh` on [Mail_VM]





          (4) If you are not familiar with remote setup, you can run `main.sh` on [Router_VM] manually.

              -> If this is your case, follow instruction below [Understanding `main.sh`] and modify `main.conf` so it will provide correct information for your environment.










#
-----------------------


### Understanding `main.sh`

#

      1. [Router_VM] user name (`$LOGNAME`) has to be exist in [Mail_VM] in order to complete remote setup.
          - In this case, we assume that `root` user is available.


          =>  [Router_VM] user name (=`$LOGNAME` = `root`)

                              -> remote setup script `mail_vm_remote_setup.sh`

                                                          -> [Mail_VM] user name (= `$LOGNAME` = `root`)





      2. `router_vm_remote_setup.sh` will provide information to `main.conf`

        (1) MAC Address of NIC on [Mail_VM]                               # In order to obtain fixed IP Address from dhcpd
              Line 73: mail_macaddr="__MAIL_MAC_ADDR__"


        (2) new mail user name & password on [Mail_VM]                    # By default, username = "nasp"/ password = "nasp"
              Line 47: new_mailuser_name="__NEW_MAIL_USER__"
              Line 48: new_mailuser_passwd="__NEW_MAIL_USER_PASSWD__"


        (3) root user (`$LOGNAME`) password on [Mail_VM]                  # For remote setup
              Line 22: ssh_passwd="__ROOT_PASSWD__"


#
-----------------------

### [ `main.sh` hierarchy ]

#

                          ~ `main.sh` ~


      main.sh                          # Main setup script to run on Router_VM
        ./main.conf                    # Variables

        hostname_setup.sh              # Setup hostname / domainanme / FQDN with Current IP Address
        base_configuration.sh          # Base packages installation / tools / VM guest Additions
        selinux_setup.sh               # Disabling SElinux

        network_setup.sh               # Disabling Firewalld / NetworkManager & Enabling sshd / Network Service
          ./net_if_setup.sh            # eth0 configuration / ifup eth0
          ./wifi_if_setup.sh           # Wifi Adapter configuration / ifup wlp0s6u1

        iptables_setup.sh              # nftables setup
          ./nftables_template.conf     # nftables template file

        nsd_setup.sh                   # nsd Setup
          ./nsd_template.conf          : configuration template file

        unbound_setup.sh               # unbound Setup
          ./unbound_template.conf      : configuration template file

        dhcpd_setup.sh                 # dhcpd Setup
         ./dhcpd_template.conf         : configuration template file

        hostapd_setup.sh               # hostapd Setup
         ./hostapd_template_wpa2.conf  : configuration template file

        mail_vm_remote_setup.sh        # Remote setup script for Mail_VM (Run by main.sh on Router_VM remotly)



#
-----------------------

### [ `main_mail_vm.sh` hierarchy ]

#

                          ~ `main_mail_vm.sh` ~


                                         # Explanation)
                                          (1) [Router_VM] will runs script `mail_vm_remote_setup.sh` by `main.sh`
                                          (2) This script will uploads `/mail_vm` directory to [Mail_VM]
                                          (3) Then, it'll trigger to run `main_mail_vm.sh` on [Mail_VM]


    /mail_vm

      ./main_mail_vm.sh                  # Main setup script - [Mail_VM]
        ./base_configuration.sh          # Base packages installation / tools / VM guest Additions
        ./selinux_setup.sh               # Disabling SElinux
        ./hostname_mail_vm.sh            # Setup hostname / domainanme / FQDN with Current IP Address

        ./network_mail_vm.sh             # Disabling Firewalld / NetworkManager & Enabling sshd / Network Service
        ./postfix_setup.sh               # postfix Setup
        ./dovecot_setup.sh               # dovecot Setup
        ./SASL_setup.sh                  # SASL Setup
        ./SSL_TLS_setup.sh               # SSL/TLS Authentication setup
