#!/bin/bash
set -o nounset  # Treat unset variables as an error

source ./main.conf

##### iptables_setup.sh #####
# nftables setup

# Install nftables
yum -y install nftables

# Enabling / Starting nftables
systemctl enable nftables.service
systemctl start nftables.service

# Replace device names in template file
sed -r -i "s/intface1/$interfacename/" ./nftables_template.conf
sed -r -i "s/input_enp/input_$interfacename/" ./nftables_template.conf

sed -r -i "s/intface2/$interface_name_eth/" ./nftables_template.conf
sed -r -i "s/input_eth/input_$interface_name_eth/" ./nftables_template.conf

sed -r -i "s/intface3/$interface_name_wlp/" ./nftables_template.conf
sed -r -i "s/input_wlp/input_$interface_name_wlp/" ./nftables_template.conf

# Flush / Override ruleset
nft -f ./nftables_template.conf
