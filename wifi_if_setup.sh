#!/bin/bash
set -o nounset  # Treat unset variables as an error

source ./main.conf


##### wifi_if_setup.sh #####
# $interface_ifcfg_wlp file configurations

## We assuming that enp0s3 is already exist
echo "IP Addressing Starting - Wifi Adapter"


## Setup for Wifi Adapter
# Create new ifcfg file for Wifi Adapter
touch $interface_ifcfg_wlp

# Change Device name
sed -r "s/$interfacename/$interface_name_wlp/" $interfaceifcfg > $interface_ifcfg_wlp


# ONBOOT = "yes"
sed -r -i 's/ONBOOT="(yes|no|)"/ONBOOT="yes"/' $interface_ifcfg_wlp
# Set DEFROUTE = no
sed -r -i 's/DEFROUTE="(yes|no|)"/DEFROUTE="no"/' $interface_ifcfg_wlp

# Delete UUID line
sed -i '/^UUID/d' $interface_ifcfg_wlp

# Delete DNS Server lines
sed -i '/^DNS1/d' $interface_ifcfg_wlp
sed -i '/^DNS2/d' $interface_ifcfg_wlp

# IP Addressing
sed -r -i "s/IPADDR=\"\b[0-9]{1,3}.\b[0-9]{1,3}.\b[0-9]{1,3}.\b[0-9]{1,3}\"/IPADDR=$newip_wlp/" $interface_ifcfg_wlp
sed -r -i "s/GATEWAY=\"\b[0-9]{1,3}.\b[0-9]{1,3}.\b[0-9]{1,3}.\b[0-9]{1,3}\"/GATEWAY=$newsubnet_wlp/" $interface_ifcfg_wlp
sed -r -i "s/PREFIX=\"\b[0-9]{1,2}\"/PREFIX=$newsubnet_wlp/" $interface_ifcfg_wlp

# Change TYPE
sed -r -i 's/TYPE="Ethernet"/TYPE="Wireless"/' $interface_ifcfg_wlp

# Add ESSID / CHANNEL / MODE / RATE

if [[ $(cat $interface_ifcfg_wlp | grep ESSID) = "" ]]; then
cat >> $interface_ifcfg_wlp << EOF
ESSID=$new_essid
CHANNEL=$new_channel
MODE=$new_mode
RATE=$new_rate
EOF

else
  continue
fi




# Turn on / Restart interface
if [[ $(cat /sys/class/net/$interface_name_wlp/operstate) == "up" ]]; then
  ifdown $interface_name_wlp
  ifup $interface_name_wlp
  echo "$interface_name_wlp has been UP"

else
  ifup $interface_name_wlp
  echo "$interface_name_wlp has been UP"
fi
