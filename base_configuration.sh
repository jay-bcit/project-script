#!/bin/bash
set -o nounset  # Treat unset variables as an error

source ./main.conf


##### base_configuration.sh #####
# This file contains basic system configurations


echo "Installing base packages"
yum -y update
echo "group_package_types=mandatory,default,optional" >> /etc/yum.conf
yum -y group install base

echo "Installing the Extra Packages for Enterprise Linux Repository"
yum -y install epel-release
yum -y update

echo "Installing project specific tools"
yum -y install curl vim wget tmux nmap-ncat tcpdump nmap git

echo "Setting Up VirtualBox Guest Additions"
echo "Installing pre-requisities"
yum -y install kernel-devel kernel-headers dkms gcc gcc-c+

echo "Creating mount point, mounting, and installing VirtualBox Guest Additions"
mkdir vbox_cd
mount /dev/cdrom ./vbox_cd
./vbox_cd/VBoxLinuxAdditions.run
umount ./vbox_cd
rmdir ./vbox_cd
