#!/bin/bash
set -o nounset  # Treat unset variables as an error


##### network_setup.sh #####
# This file contains basic system configurations

source ./main.conf


# Call net_if_setup.sh
source ./net_if_setup.sh

# Call wifi_if_setup.sh
source ./wifi_if_setup.sh

# Call quagga_setup.sh
source ./quagga_setup.sh

# Enable routing
echo "Enabling routing on Centos7"
if [[ $var_11 == "on" ]] && [[ $(cat /etc/sysctl.conf | grep net.ipv4.ip_forward) == "" ]]; then
  echo "net.ipv4.ip_forward = 1" >> /etc/sysctl.conf
  printf "SYSTEM: $(sysctl -p /etc/sysctl.conf)"
  echo "Enabling routing on Centos7 Comeplted"
else
  continue
fi


# Disabling Firwalld
echo "Disabling firewalld"
systemctl stop firewalld.service
systemctl disable firewalld.service
echo "Disabling firewalld Completed"

# Disabling NetworkManager
echo "Disabling Network Manager"
systemctl stop NetworkManager.service
systemctl disable NetworkManager.service
echo "Disabling Network Manager Completed"

# Enabling Network Service
echo "Enabling / Starting Network Service"
systemctl enable network.service
systemctl start network.service
echo "Enabling / Starting Network Service Completed"

# Enabling sshd Service
echo "Enabling sshd"
systemctl enable sshd.service
systemctl start sshd.service
echo "Enabling sshd Completed"
