#!/bin/bash
set -o nounset  # Treat unset variables as an error

source ./main.conf

##### dhcpd_setup.sh #####
# dhcpd setup

# Install dhcpd
yum -y install dhcp


# 1.Modifying dhcpd_template.conf
sed -r -i "s/\sdomain-name\s\"TLD_HERE\";/ domain-name \"$TLD_HERE\";/" ./dhcpd_template.conf
sed -r -i "s/\sdomain-name-servers.*$/ domain-name-servers $currentip_eth;/" ./dhcpd_template.conf



  # group settings for $interface_name_eth network
sed -r -i "s/\s__interface_name_eth__/ $interface_name_eth/" ./dhcpd_template.conf
sed -r -i "s/\s__networkid_eth__/ $networkid_eth/" ./dhcpd_template.conf
sed -r -i "s/\s__netmask_eth__/ $netmask_eth/" ./dhcpd_template.conf
sed -r -i "s/\s__currentip_eth__/ $currentip_eth/" ./dhcpd_template.conf
sed -r -i "s/\s__networkid_3f__/ $networkid_3f/" ./dhcpd_template.conf
sed -r -i "s/\s__networkid_3f_1__/ $networkid_3f/" ./dhcpd_template.conf
sed -r -i "s/__strting_ip_addr_range_eth__/$strting_ip_addr_range_eth /" ./dhcpd_template.conf
sed -r -i "s/__ending_ip_addr_range_eth__/$ending_ip_addr_range_eth/" ./dhcpd_template.conf
sed -r -i "s/\s__mail_hostname__/ $mail_hostname/" ./dhcpd_template.conf
sed -r -i "s/\s__mail_macaddr__/ $mail_macaddr/" ./dhcpd_template.conf
sed -r -i "s/\s__mail_hostip__/ $mail_hostip/" ./dhcpd_template.conf

  # group settings for $interface_name_wlp network
sed -r -i "s/\s__interface_name_wlp__/ $interface_name_wlp/" ./dhcpd_template.conf
sed -r -i "s/\s__networkid_wlp__/ $networkid_wlp/" ./dhcpd_template.conf
sed -r -i "s/\s__netmask_wlp__/ $netmask_wlp/" ./dhcpd_template.conf
sed -r -i "s/__strting_ip_addr_range_wlp__/$strting_ip_addr_range_wlp /" ./dhcpd_template.conf
sed -r -i "s/__ending_ip_addr_range_wlp__/$ending_ip_addr_range_wlp /" ./dhcpd_template.conf



## Copy template to /etc/dhcpd
cp ./dhcpd_template.conf /etc/dhcp/dhcpd.conf




# 2. Modifying dhcpd.service files
if [[ $(ls /etc/systemd/system | grep dhcpd) == "dhcpd.service" ]]; then
  sed -r -i "s/--no-pid.*$/--no-pid\t$interface_name_eth $interface_name_wlp/" /etc/systemd/system/dhcpd.service
else
  sed -r -i "s/--no-pid.*$/--no-pid\t$interface_name_eth $interface_name_wlp/" /lib/systemd/system/dhcpd.service
fi



# Enabling / Starting dhcpd
systemctl enable dhcpd.service
systemctl start dhcpd.service


## Restart dhcpd.service
systemctl restart dhcpd.service

## Restart network.service
systemctl restart network.service
