#!/bin/bash
set -o nounset  # Treat unset variables as an error

source ./main.conf


##### hostname_setup.sh #####
# This file contains basic system configurations


if [[ $(hostname -f) != "$newhostname.$newdomainname" ]]; then

  # 1. Modifying hostaname
  #hostnamectl set-hostname Project --no-ask-password
  echo "New hostname: $newhostname"
  echo "$newhostname" > /etc/hostname


  # 2. Test if 'search domainname' statement is exist in /etc/resolv.conf, Add search domain
  echo "New search domainname: $newdomainname"
# echo $(cat /etc/resolv.conf) | grep --quiet "$newdomainname"
#  if [ $? = 1 ]; then
  if [[ $(echo $(cat /etc/resolv.conf) | grep --quiet "$newdomainname") == "" ]]; then
  echo "search $newdomainname" >> /etc/resolv.conf
  fi


  # 3. Test if alias statement is exist in /etc/hosts, Add alias
  echo "New alias in /etc/hosts"
  echo $(cat /etc/hosts) | grep --quiet "$newhostname.$newdomainname"
  if [ $? = 1 ]; then
    echo "$currentip $newhostname.$newdomainname $newhostname"  >> /etc/hosts
  fi

  # 4. Reload hostname file
  hostname -F /etc/hostname

else

  echo -e "[System]: Current hostname is correctly set => $(hostname -f)"



fi
