#!/bin/bash
set -o nounset  # Treat unset variables as an error

source ./main.conf


##### unbound_setup.sh #####
# unbound setup



# Install unbound
if [[ $(rpm -qa | grep unbound) = "" ]]; then
  yum -y install unbound
else
  continue
fi


# 1.Modifying unbound_template.conf
sed -r -i "s/\sinterface:\seth_INTFACE/\tinterface: $currentip_eth/" ./unbound_template.conf
sed -r -i "s/\sinterface:\sWifi_ADAPTER/\tinterface: $currentip_wlp/" ./unbound_template.conf
sed -r -i "s/\sinterface:\sLOCALHOST/\tinterface: $lchost/" ./unbound_template.conf
sed -r -i "s/\saccess-control:\sNEW_ACCESS/\taccess-control: $networkid_eth\/$currentprefix allow/" ./unbound_template.conf
sed -r -i "s/\sprivate-domain:\s\"TLD_HERE\"/\tprivate-domain: \"$TLD_HERE\"/" ./unbound_template.conf

## Adding Stub zones
sed -r -i "s/\sname:\s\"NEW_DOMAIN_NAME\"/\tname: \"$newdomainname\"/" ./unbound_template.conf
sed -r -i "s/\sname:\s\"NEW_REVERSE_LOOKUP_ZONE\"/\tname: \"$reverse_lookupzone_eth.in-addr.arpa\"/" ./unbound_template.conf
sed -r -i "s/\sstub-addr:\senp_ADAPTER/\tstub-addr: $currentip/" ./unbound_template.conf

## Overide unbound.conf
cp ./unbound_template.conf /etc/unbound/unbound.conf




# ?. Modifying ifcfg-enp0s3 to lookup localhost
sed -r -i "s/DNS1=\"\b[0-9]{1,3}.\b[0-9]{1,3}.\b[0-9]{1,3}.\b[0-9]{1,3}\"/DNS1=$lchost/" $interfaceifcfg
sed -r -i "s/DNS2=\"\b[0-9]{1,3}.\b[0-9]{1,3}.\b[0-9]{1,3}.\b[0-9]{1,3}\"//" $interfaceifcfg

# ??. Modifying /etc/resolv.conf to lookup localhost
## Remove lastline in the file = deleting nameserver 8.8.8.8
sed -i -e '$ d' /etc/resolv.conf

# Enabling / Starting unbound
systemctl enable unbound.service
systemctl start unbound.service

## Restart interface
ifdown $interfacename
ifup $interfacename

## Restart unbound.service
systemctl restart unbound.service
