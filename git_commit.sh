#!/bin/bash
set -o nounset

git init
git add -A
git commit -m "Testing"
git remote add prj https://jay-bcit@bitbucket.org/jay-bcit/project-script.git
git push prj master
